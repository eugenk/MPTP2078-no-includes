% Mizar problem: t190_relat_1,relat_1,2787,59 
fof(t190_relat_1, conjecture,  (! [A] :  (! [B] :  (v1_relat_1(B) =>  ~ ( ( ~ (v3_relat_1(k7_relat_1(B, A)))  & v3_relat_1(B)) ) ) ) ) ).
fof(dt_k7_relat_1, axiom,  (! [A, B] :  (v1_relat_1(A) => v1_relat_1(k7_relat_1(A, B))) ) ).
fof(dt_k7_relat_1, axiom,  (! [A, B] :  (v1_relat_1(A) => v1_relat_1(k7_relat_1(A, B))) ) ).
fof(fc19_relat_1, axiom,  (! [A, B] :  ( (v1_relat_1(A) & v3_relat_1(A))  =>  (v1_relat_1(k7_relat_1(A, B)) & v3_relat_1(k7_relat_1(A, B))) ) ) ).
